from pdf2image import convert_from_bytes


class PdfToImageService:

    @classmethod
    def convert(cls, pdf, dpi):
        return convert_from_bytes(pdf_file=pdf, dpi=dpi)
