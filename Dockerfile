FROM continuumio/miniconda3:latest

COPY ./app.py ./app.py
COPY ./pdf_to_image_service.py ./pdf_to_image_service.py
COPY ./pdf2image-server-env.txt ./pdf2image-server-env.txt

RUN conda create --file ./pdf2image-server-env.txt --name pdf2image-server

# Make RUN commands use the new environment:
SHELL ["conda", "run", "-n", "myenv", "/bin/bash", "-c"]


CMD ["conda", "run", "--no-capture-output", "-n", "pdf2image-server", "uvicorn", "app:app", "--host", "0.0.0.0", "--port", "80"]