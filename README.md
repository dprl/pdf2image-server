# pdf2image-server
The purpose of this project is to be a scalable service capable of converting PDFs to images.

## Installation 
The anaconda python environment manager must be installed.
run 
```shell
$ conda create create --file ./pdf2image-server-env.txt --name pdf2image-server
$ conda activate pdf2image-server
```
## Running the Server Locally
To start the server locally run
```shell
(pdf2image-server) $ uvicorn app:app --host 0.0.0.0 --port 8000
```
Then go to `localhost:8000/docs` to interact with the swagger documentation UI.
## Running the Server with Docker
Docker must first be installed on the system. 
```shell
$ docker pull dprl/pdf2image-server:latest
$ docker run -p 8000:80 dprl/pdf2image-server:latest
```
Then go to `localhost:8000/docs` to interact with the swagger documentation UI.
## Provided Client Code
The value returned by the convert endpoint is a zip file containing individual images of each page in the pdf.
Provided in the `test_client.py` file is a script that makes an async call to a running server and then extracts and
iterates over the returned images and displays them. This code can be a starting point for other applications.
