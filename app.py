from io import BytesIO
import zipfile
from fastapi import FastAPI, File, Response
import uvicorn

from pdf_to_image_service import PdfToImageService

app = FastAPI()


@app.post("/convert/{dpi}")
async def convert(dpi: int, file: bytes = File(...)):
    images = PdfToImageService.convert(dpi=dpi, pdf=file)
    zip_io = BytesIO()
    zipped_images = zipfile.ZipFile(zip_io, "w")
    for i, img in enumerate(images):
        image_bytes = BytesIO()
        img.save(image_bytes, format=img.format)
        zipped_images.writestr(str(i)+".png", image_bytes.getvalue(), compress_type=zipfile.ZIP_DEFLATED)
    zipped_images.close()
    resp = Response(zip_io.getvalue(), media_type="application/x-zip-compressed", headers={
        'Content-Disposition': f'attachment;filename={f"pdf2img_{dpi}.zip"}'
    })
    return resp

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
