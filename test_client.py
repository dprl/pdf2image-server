import argparse
import asyncio
from io import BytesIO
import aiohttp
import zipfile
from PIL import Image
import matplotlib.pyplot as plt
import math


async def make_request(pdf_file_name, dpi=256):
    opened_file = open(pdf_file_name, 'rb')
    file = {"file": opened_file}
    r = f"http://localhost:8000/convert/{dpi}"
    photos = []
    async with aiohttp.ClientSession() as session:
        async with session.post(r, data=file) as resp:
            response_bytes = await resp.read()
            io_bytes = BytesIO(response_bytes)
            with zipfile.ZipFile(io_bytes) as zip_file:
                for name in zip_file.namelist():
                    data = zip_file.read(name)
                    img = Image.open(BytesIO(data))
                    photos.append(img)

    n_col = math.ceil(math.sqrt(len(photos)))
    n_row = math.ceil(len(photos)/n_col)

    _, axs = plt.subplots(n_row, n_col, figsize=(12, 10))
    axs = axs.flatten()
    for img, ax in zip(photos, axs):
        ax.imshow(img)
    plt.show()




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='convert pdf to images.')
    parser.add_argument('--file', help='Path to pdf to convert', default='/home/mattlangsenkamp/Documents/dprl/pdf2image-server/test_pdfs/largerExample.pdf')
    args = parser.parse_args()
    file_name = args.file

    loop = asyncio.get_event_loop()
    loop.run_until_complete(make_request(file_name))

